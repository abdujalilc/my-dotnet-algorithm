﻿SomeDelegate _delegate = Foo;
_delegate();
_delegate += new SomeDelegate(Goo);
_delegate();
_delegate += Sue;
_delegate();

Console.Read();

static void Foo()
{
    Console.WriteLine("Foo()");
}

static void Goo()
{
    Console.WriteLine("Goo()");
}

static void Sue()
{
    Console.WriteLine("Soo()");
}
delegate void SomeDelegate();