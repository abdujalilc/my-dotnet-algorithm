﻿namespace DynamicCreate
{
    /// <summary>
    ///Animal
    /// </summary>
    public interface IAnimal
    {
        /// <summary>
        ///Call
        /// </summary>
        public void Cry();
    }
}