﻿namespace DynamicCreate
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var types = AppDomain.CurrentDomain.GetAssemblies()
                                    .SelectMany(a => a.GetTypes().Where(t => t.GetInterfaces().Contains(typeof(IAnimal))))
                                    .ToList();

            foreach (Type t in types)
            {
                Console.WriteLine(t.Name);
            }

            foreach (Type t in types)
            {
                IAnimal animal;
                if (t.GetConstructors().Any(x => x.GetParameters().Any()))
                {
                    //Parameterized constructor
                    animal = (IAnimal)Activator.CreateInstance(t, new object[] { "Ah Huang" })!;
                }
                else
                {
                    //non-parameter constructor 
                    animal = (IAnimal)Activator.CreateInstance(t, new object[] { })!;
                }
                animal.Cry();
            }
        }
    }
}
