﻿namespace _20_delegate_vs_event
{
    internal class Program
    {
        static void Main(string[] args)
        {

            WhyUseEvents why = new WhyUseEvents();
            why.MyLoggingDelegate += () =>
            {
                Console.WriteLine("Hi I am subscribing to the delegate.");
            };

            why.MyLoggingDelegate.Invoke();  //Team Client here can directly invoke the delegate! That is not good.

            //why.MyLoggingEvent.Invoke(); //now the team client can't invoke directly. Invoke is protected by event
        }
    }

    public class WhyUseEvents
    {
        public delegate void YourLoggingHere();

        public YourLoggingHere MyLoggingDelegate;
        public event YourLoggingHere MyLoggingEvent;
        public void MyMainProgram()
        {
            ExecuteValidation();
        }

        private void ExecuteValidation()
        {
            if (MyLoggingDelegate != null)
            {
                MyLoggingDelegate.Invoke();
            }

            MyLoggingEvent.Invoke();
        }
    }
}