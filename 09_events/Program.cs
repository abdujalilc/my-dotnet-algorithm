﻿public class Program
{
    public static void Main()
    {
        ProcessBusinessLogic bl = new ProcessBusinessLogic();
        bl.ProcessCompleted += bl_ProcessCompleted;
        //invokes process completed
        bl.StartProcess();
        Console.ReadLine();
    }

    static void bl_ProcessCompleted(string test, int number)
    {
        Console.WriteLine(test);
        Console.WriteLine(number);
    }
}

public delegate void Notify(string test, int number);
public class ProcessBusinessLogic
{
    public event Notify? ProcessCompleted;
    public void StartProcess()
    {
        Console.WriteLine("Process Started!");
        OnProcessCompleted();
    }
    protected virtual void OnProcessCompleted()
    {
        ProcessCompleted?.Invoke("Hello World", 10);
    }
}