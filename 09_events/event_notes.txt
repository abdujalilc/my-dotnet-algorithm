﻿Events - Publisher subscriber model
We can use events to create a pure publisher / subscriber model.


Every class which uses your delegate declared class 
can invoke the delegate directly.


using events, the compiler protects your fields from unwanted access