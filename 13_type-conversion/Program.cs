﻿namespace _13_type_conversion
{
    internal class Program
    {
        public static void Display(Square square)
        {
            Console.WriteLine($"Sides: {square.Sides}");
        }
        static void Main(string[] args)
        {
            Shape shape = new Square();
            shape.Sides = 3;
            Display((Square)shape);
            Console.ReadKey();
        }
    }
    class Shape
    {
        public int Sides { get; set; }
    }
    class Square : Shape
    {

    }
}