﻿List<Printer> printers = new List<Printer>();

int i = 0;

for (; i < 10; i++)
{
    printers.Add(delegate { Console.WriteLine(i); });
}

foreach (var prnt in printers)
{
    prnt(i);
}

Printer printer = new Printer(test);

void test(int i)
{
    Console.WriteLine(i);
}

printer(15);

Console.ReadKey();

internal delegate void Printer(int i);

