﻿
string str = "hello";
Object str2 = str;

Console.WriteLine("Using Equality operator: {0}", str == str2);

Console.WriteLine("Using equals() method: {0}", str.Equals(str2));
//Console.ReadKey();

string str3 = new string("hello");
char[] values = { 'h', 'e', 'l', 'l', 'o' };
object str4 = new string(values);
Console.WriteLine("Using Equality operator: {0}", str3 == str4);
Console.WriteLine("Using equals() method: {0}", str3.Equals(str4));
Console.ReadKey();