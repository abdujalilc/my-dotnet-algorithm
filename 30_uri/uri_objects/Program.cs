﻿using System.Web;

Uri uri = new Uri("http://danpatrascu.com/SomeArea/SomeController/SomeAction?query1=somevalue&query2=lalala");

Console.WriteLine("Host: " + uri.Host);
Console.WriteLine("Port: " + uri.Port);
Console.WriteLine("PathAndQuery: " + uri.PathAndQuery);
Console.WriteLine("Scheme: " + uri.Scheme);
Console.WriteLine("AbsolutePath: " + uri.AbsolutePath);
Console.WriteLine("AbsoluteUri: " + uri.AbsoluteUri);
Console.WriteLine("Authority: " + uri.Authority);
Console.WriteLine("OriginalString: " + uri.OriginalString);
Console.WriteLine("Query: " + uri.Query);
Console.WriteLine("LocalPath: " + uri.LocalPath);
Console.WriteLine("UserInfo: " + uri.UserInfo);

string originalString = "http://ac1cb2.wiut.uz/students#Студенты:ПолучитьСведенияПоСтудентам";

// Using HttpUtility.UrlEncode
string encodedString1 = HttpUtility.UrlEncode(originalString);
Console.WriteLine("Encoded using HttpUtility.UrlEncode: " + encodedString1);

// Using Uri.EscapeDataString
string encodedString2 = Uri.EscapeDataString(originalString);
Console.WriteLine("Encoded using Uri.EscapeDataString: " + encodedString2);