﻿using FuzzyString;

public sealed class Program
{
    public static void Main()
    {
        string str1 = "atb302";
        string str2 = "atb 302l";
        string str3 = "atb_302tg";

        Console.WriteLine($"Similarity between '{str1}' and '{str2}': {str1.LevenshteinDistance(str2):F2}%");
        Console.WriteLine($"Similarity between '{str1}' and '{str3}': {str1.LevenshteinDistance(str3):F2}%");
        Console.WriteLine($"Similarity between '{str2}' and '{str3}': {str2.LevenshteinDistance(str3):F2}%");
    }
}
