﻿
void UseParams(params int[] list)
{
    for (int i = 0; i < list.Length; i++)
    {
        Console.Write(list[i] + " ");
    }
    Console.WriteLine();
}

void UseParams2(params object[] list)
{
    for (int i = 0; i < list.Length; i++)
    {
        Console.Write(list[i] + " ");
    }
    Console.WriteLine();
}

// usage
UseParams(1, 2, 3, 4);
UseParams2(1, 'a', "test");

Console.ReadKey();

TestStatic.TestMethod();


static class TestStatic
{
    public static void TestMethod()
    {

    }
}