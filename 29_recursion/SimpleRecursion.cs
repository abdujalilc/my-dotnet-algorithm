﻿namespace _29_recursion
{
    internal class SimpleRecursion
    {
        internal static int CalculateSumRecursively(int n, int m)
        {
            int sum = n;
            if (n < m)
            {
                n++;
                return sum += CalculateSumRecursively(n, m);
            }
            return sum;
        }
    }
}
