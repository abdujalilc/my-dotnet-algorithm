﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _29_recursion.Model
{
    public interface IGroup
    {
        string ID { get; set; }
        string Name { get; set; }
        string ParentID { get; set; }
        List<IGroup> Groups { get; set; }
    }
    public class Group : IGroup
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string ParentID { get; set; }
        public List<IGroup> Groups { get; set; }
        public Group()
        {

        }
        public Group(string id, string name, List<IGroup> childs)
        {
            ID = id;
            Name = name;
            Groups = (List<IGroup>)childs.Cast<IGroup>();
        }

    }
}
