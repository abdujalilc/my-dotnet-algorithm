﻿Func<string, string> greet = delegate (string name)
{
    return "Hi " + name;
};

Console.WriteLine(greet("Jack"));


Action<int> DoSomeThing = i => Console.WriteLine(i);

Action DoSomeThing2 = () => Console.WriteLine("Action with no parameters.");

DoSomeThing(10);
DoSomeThing2();


Predicate<int> GetBool = delegate (int val)
  {
      if (val <= 0)
          return false;
      else
          return true;
  };

Console.WriteLine(GetBool(100));

Console.ReadLine();