﻿namespace DelegateLambdaExFuncActPredDemo
{
    class Program4_LambdaExp
    {
        public const double PI = 3.14;
        delegate double CalculateCircumferencePointer(double result, string test);
        static void Main(string[] args)
        {
            CalculateCircumferencePointer cpointer = (radius,test) => 2 * PI * radius;
            double result = cpointer(12,"test");
            Console.WriteLine($"Alana uzunligi: {result}");
            Console.Read();
        }
    }
}
/*anonymous
    CalculateCircumferencePointer cpointer2 = delegate (double radius) { return 2 * PI * radius; };
 */

/*
 // Lambda expression with void return type
    VoidDelegate voidLambda = (someString, someInt) => Console.WriteLine("This lambda has a 'void' return type");
 */
