# Dotnet Questions Web

This repository contains various C# and .NET coding exercises, categorized by concepts.

## Project Structure

- **01_string-reverse/** - String reversal implementations.
- **02_object-equals/** - Object equality comparisons.
- **03_delegate/** - Delegates in C#.
- **04_function-params/** - Function parameters and their behaviors.
- **05_test-static/** - Static class and method behaviors.
- **06_task-await/** - Asynchronous programming using Task and Await.
- **07_cancellation-token/** - Cancellation tokens in async operations.
- **08_delegate-chain/** - Chaining delegates.
- **09_events/** - Event handling in C#.
- **10_dynamic-create/** - Dynamic object creation.
- **11_function-action-predicate/** - Function, Action, and Predicate usage.
- **12_generics/** - Generics in C#.
- **13_type-conversion/** - Type conversion techniques.
- **14_virtual-override/** - Virtual methods and method overriding.
- **15_value-reference/** - Value types vs. reference types.
- **16_struct-box-unbox/** - Boxing and unboxing of structs.
- **17_struct-boxing-interface/** - Struct and interface boxing.
- **18_type-members/** - Type members in C#.
- **19_struct-instance/** - Struct instance behavior.
- **20_delegate-vs-event/** - Difference between delegates and events.
- **21_generic-vs-nongeneric/** - Generic vs non-generic implementations.
- **22_command-line-args/** - Handling command-line arguments.
- **23_closedtype-opentype/** - Open and closed generic types.
- **24_char-convert/** - Character conversion methods.
- **25_string-compare/** - String comparison techniques.
- **26_string-encode-decode/** - Encoding and decoding strings.
- **27_timer-call-back/** - Timer callbacks in C#.
- **28_attributes/** - Attributes in C#.
- **29_recursion/** - Recursive function implementations.
- **30_url/url_objects/** - Handling URLs and objects.

## Setup & Usage

1. Clone the repository:
   ```sh
   git clone https://gitlab.com/your-repo/dotnet-questions-web.git
   ```
2. Open the project in Visual Studio.
3. Run the relevant C# examples.

## Contribution

Contributions are welcome! Feel free to fork the repository and submit a pull request.

## License

This project is licensed under the MIT License.
